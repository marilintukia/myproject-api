DROP TABLE IF EXISTS `childskillgroup`;
DROP TABLE IF EXISTS `child`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `exercisepictures`;
DROP TABLE IF EXISTS `exercise`;
DROP TABLE IF EXISTS `skills`;
DROP TABLE IF EXISTS `skillgroup`;

CREATE TABLE IF NOT EXISTS `skillgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `omandatudOskus` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `textDescription` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ageGroup` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skillName` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skillgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_skills_skillgroup` (`skillgroup_id`),
  CONSTRAINT `FK_skills_skillgroup` FOREIGN KEY (`skillgroup_id`) REFERENCES `skillgroup` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `exercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exerciseName` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skills_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_exercise_skills` (`skills_id`),
  CONSTRAINT `FK_exercise_skills` FOREIGN KEY (`skills_id`) REFERENCES `skills` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `exercisepictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pictureName` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pictureDescription` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exercise_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_exercisepictures_exercise` (`exercise_id`),
  CONSTRAINT `FK_exercisepictures_exercise` FOREIGN KEY (`exercise_id`) REFERENCES `exercise` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `child` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'https://lh4.googleusercontent.com/proxy/4o2xqHZnxrf4JsMkBYg8XHvlag_onE0XHJpdn1nyFVbJrohoJFYSulbcfaXS7Llp1cVaqehfzmL4U0nb93xTRh9HocKWBPuGZnJRaGVBZ87JmI33RXuxGw',
  `age` date NOT NULL,
  `prematurity` BIT NOT NULL,
  `trueAgeInWeeks` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_child_user` (`user_id`),
  CONSTRAINT `FK_child_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `childskillgroup` (
  `child_id` int(11) NOT NULL,
  `skillgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`child_id`,`skillgroup_id`),
  KEY `FK_childskillgroup_skillgroup` (`skillgroup_id`),
  CONSTRAINT `FK_childskillgroup_laps` FOREIGN KEY (`child_id`) REFERENCES `child` (`id`),
  CONSTRAINT `FK_childskillgroup_skillgroup` FOREIGN KEY (`skillgroup_id`) REFERENCES `skillgroup` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;