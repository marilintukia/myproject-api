package com.valiit.listedcompaniesapi.service;

import com.valiit.listedcompaniesapi.model.ChildRegistration;
import com.valiit.listedcompaniesapi.model.Exercise;
import com.valiit.listedcompaniesapi.model.SkillGroup;
import com.valiit.listedcompaniesapi.repository.ChildRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SkillService {
    @Autowired
    private ChildRepository childRepository;

    public List<SkillGroup> getSkillGroups() {
        return childRepository.getSkillGroups();
    }

    public List<Exercise> getExercises(int ageInMonths) {
        List<Exercise> exercises = new ArrayList<>();
        exercises.addAll(childRepository.getExercises(ageInMonths));

        boolean previousGroupExists = previousAgeGroupExists(ageInMonths);
        if (previousGroupExists) {
            int previousAgeGroupStartMonths = getPreviousAgeGroupStartInMonths(ageInMonths);
            exercises.addAll(childRepository.getExercises(previousAgeGroupStartMonths));
        }

        return exercises;
    }

    private boolean previousAgeGroupExists(int ageInMonths) {
        return ageInMonths > 3 && ageInMonths < 18;
    }

    private int getPreviousAgeGroupStartInMonths(int ageInMonths) {
        int previousAgeGroupStartMonths = 0;
        if (ageInMonths < 7) {
            previousAgeGroupStartMonths = 0;
        } else if (ageInMonths < 10) {
            previousAgeGroupStartMonths = 4;
        } else if (ageInMonths < 18) {
            previousAgeGroupStartMonths = 7;
        }

        return previousAgeGroupStartMonths;
    }

    public List<ChildRegistration> getAllChildren() {
        return childRepository.getAllChildren();
    }

    public void deleteChild(int id) {
        if (id > 0) {
            childRepository.deleteChild(id);
        }
    }

    public void deleteSkill(int child_id, int skillgroup_id) {
        if (child_id > 0) {
            childRepository.deleteSkill(child_id, skillgroup_id);
        }
    }

    public void addExerciseCount(int child_id, int skillgroup_id) {
        boolean countEntityExists = childRepository.exerciseCountExists(child_id, skillgroup_id);
        if (countEntityExists == true){
            int count = childRepository.getCount(child_id, skillgroup_id);
            childRepository.updateExercise(count + 1, child_id, skillgroup_id);
        } else  childRepository.addExercise (child_id, skillgroup_id);
    }
}