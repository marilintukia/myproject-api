package com.valiit.listedcompaniesapi.rest;

import com.valiit.listedcompaniesapi.dto.CompanyDto;
import com.valiit.listedcompaniesapi.model.User;
import com.valiit.listedcompaniesapi.repository.UserRepository;
import com.valiit.listedcompaniesapi.service.CompanyService;
import com.valiit.listedcompaniesapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
@CrossOrigin("*")
public class CompanyController {

    @Autowired
    private CompanyService companyService;
    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public List<CompanyDto> getCompanies() {
        return companyService.getCompanies();
    }

    private String getUser() {
        String username =
                SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.getUserByUsername(username);
        return username;
    }

    @GetMapping("/{id}")
    public CompanyDto getCompany(@PathVariable("id") int id) {
        return companyService.getCompany(id);
    }

    @PostMapping
    public void saveCompany(@RequestBody CompanyDto companyDto) {
        companyService.saveCompany(companyDto);
    }

    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable("id") int id) {
        companyService.deleteCompany(id);
    }
}

