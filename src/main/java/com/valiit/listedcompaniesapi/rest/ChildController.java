package com.valiit.listedcompaniesapi.rest;

import com.valiit.listedcompaniesapi.dto.CompanyDto;
import com.valiit.listedcompaniesapi.model.ChildRegistration;
import com.valiit.listedcompaniesapi.model.Exercise;
import com.valiit.listedcompaniesapi.model.SkillGroup;
import com.valiit.listedcompaniesapi.repository.ChildRepository;
import com.valiit.listedcompaniesapi.service.SkillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/child")
@CrossOrigin("*")
public class ChildController {

    @Autowired
    private ChildRepository childRepository;

    @Autowired
    private SkillService skillService;

    @PostMapping
    public void saveChild(@RequestBody ChildRegistration childRegistration) {
        childRepository.addChild(childRegistration);
    }


    @PutMapping
    public void updateChild(@RequestBody ChildRegistration childRegistration) {
        childRepository.updateChild(childRegistration);
    }

    @GetMapping("/skillgroups")
    public List<SkillGroup> getSkillGroups() {
        return skillService.getSkillGroups();
    }

    @GetMapping("/exercises/{age_in_months}")
    public List<Exercise> getExercises(@PathVariable("age_in_months") int ageInMonths){
        return skillService.getExercises(ageInMonths);
    }

    @GetMapping
    public List<ChildRegistration> getAllChildren() {
        return skillService.getAllChildren();
    }

    @DeleteMapping("/{id}")
    public void deleteChild(@PathVariable("id") int id) {
        skillService.deleteChild(id);
    }

    @PostMapping("/skill/{child_id}/{skillgroup_id}")
    public void saveSkill(@PathVariable("child_id") int childId, @PathVariable("skillgroup_id") int skillgroupId) {
        childRepository.saveSkill(childId, skillgroupId);
    }
    @DeleteMapping("/skill/{child_id}/{skillgroup_id}")
    public void deleteSkill(@PathVariable("child_id") int childId, @PathVariable("skillgroup_id") int skillgroupId) {
        skillService.deleteSkill(childId, skillgroupId);
    }
    @PostMapping("/exercisecount/{child_id}/{exercise_id}")
    public void addExerciseCount(@PathVariable("child_id") int childId, @PathVariable("exercise_id") int exerciseId) {
        skillService.addExerciseCount(childId, exerciseId);
    }
}
