package com.valiit.listedcompaniesapi.model;

public class ExerciseDescription {
    private Integer id;
    private String pictureName;
    private String pictureDescription;
    private Integer exercise_id;

    public ExerciseDescription(){}

    public ExerciseDescription(Integer id, String pictureName, String exerciseDescription, Integer exercise_id) {
        this.id = id;
        this.pictureName = pictureName;
        this.pictureDescription = exerciseDescription;
        this.exercise_id = exercise_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPictureName() {
        return pictureName;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public String getPictureDescription() {
        return pictureDescription;
    }

    public void setPictureDescription(String pictureDescription) {
        this.pictureDescription = pictureDescription;
    }

    public Integer getExercise_id() {
        return exercise_id;
    }

    public void setExercise_id(Integer exercise_id) {
        this.exercise_id = exercise_id;
    }
}
