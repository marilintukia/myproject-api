package com.valiit.listedcompaniesapi.model;

import java.time.LocalDate;
import java.util.List;

public class ChildRegistration {
    private Integer id;
    private String firstName;
    private String lastName;
    private String picture;
    private LocalDate age;
    private Integer prematurity;
    private Integer trueAgeInWeeks;
    private Integer prematurityAgeInWeeks;
    private List<SkillGroup> childSkills;
    private Integer minApplicableExerciseEndMonth;
    private List<ExerciseCount> exerciseCount;

    public ChildRegistration() {
    }

    public ChildRegistration(Integer id, String firstName, String lastName, String picture, LocalDate age, Integer prematurity, Integer trueAgeInWeeks, Integer prematurityAgeInWeeks, List<SkillGroup> childSkills, Integer minApplicableExerciseEndMonth, List<ExerciseCount> exerciseCount) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.picture = picture;
        this.age = age;
        this.prematurity = prematurity;
        this.trueAgeInWeeks = trueAgeInWeeks;
        this.prematurityAgeInWeeks = prematurityAgeInWeeks;
        this.childSkills = childSkills;
        this.minApplicableExerciseEndMonth = minApplicableExerciseEndMonth;
        this.exerciseCount = exerciseCount;
    }

    public List<SkillGroup> getChildSkills() {
        return childSkills;
    }

    public void setChildSkills(List<SkillGroup> childSkills) {
        this.childSkills = childSkills;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public LocalDate getAge() {
        return age;
    }

    public void setAge(LocalDate age) {
        this.age = age;
    }

    public Integer getPrematurity() {
        return prematurity;
    }

    public void setPrematurity(Integer prematurity) {
        this.prematurity = prematurity;
    }

    public Integer getTrueAgeInWeeks() {
        return trueAgeInWeeks;
    }

    public void setTrueAgeInWeeks(Integer trueAgeInWeeks) {
        this.trueAgeInWeeks = trueAgeInWeeks;
    }

    public Integer getPrematurityAgeInWeeks() {
        return prematurityAgeInWeeks;
    }

    public void setPrematurityAgeInWeeks(Integer prematurityAgeInWeeks) {
        this.prematurityAgeInWeeks = prematurityAgeInWeeks;
    }

    public Integer getMinApplicableExerciseEndMonth() {
        return minApplicableExerciseEndMonth;
    }

    public void setMinApplicableExerciseEndMonth(Integer minApplicableExerciseEndMonth) {
        this.minApplicableExerciseEndMonth = minApplicableExerciseEndMonth;
    }

    public List<ExerciseCount> getExerciseCount() {
        return exerciseCount;
    }

    public void setExerciseCount(List<ExerciseCount> exerciseCount) {
        this.exerciseCount = exerciseCount;
    }

    public static class ExerciseCount {
        private Integer exercise_id;
        private Integer count;


        public Integer getExercise_id() {
            return exercise_id;
        }

        public void setExercise_id(Integer exercise_id) {
            this.exercise_id = exercise_id;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }
    }
}

