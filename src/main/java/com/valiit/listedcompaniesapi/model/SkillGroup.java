package com.valiit.listedcompaniesapi.model;

import java.util.List;

public class SkillGroup {
    private Integer id;
    private String omandatudOskus;
    private String textDescription;
    private String ageGroup;
    private List<Skill> skills;

    public SkillGroup(){
    }

    public SkillGroup(Integer id, String omandatudOskus, String textDescription, String ageGroup, List<Skill> skills) {
        this.id = id;
        this.omandatudOskus = omandatudOskus;
        this.textDescription = textDescription;
        this.ageGroup = ageGroup;
        this.skills = skills;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOmandatudOskus() {
        return omandatudOskus;
    }

    public void setOmandatudOskus(String omandatudOskus) {
        this.omandatudOskus = omandatudOskus;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(String textDescription) {
        this.textDescription = textDescription;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }
}
