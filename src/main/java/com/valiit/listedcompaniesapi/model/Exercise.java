package com.valiit.listedcompaniesapi.model;

import ch.qos.logback.core.util.InvocationGate;

import java.util.List;

public class Exercise {
    private Integer id;
    private String exerciseName;
    private Integer skills_id;
    private Integer skillgroup_id;
    private List <ExerciseDescription> exerciseDescriptions;

    public Exercise(){}

    public Exercise(Integer id, String exerciseName, Integer skills_id, Integer skillgroup_id, List<ExerciseDescription> exerciseDescriptions) {
        this.id = id;
        this.exerciseName = exerciseName;
        this.skills_id = skills_id;
        this.skillgroup_id = skillgroup_id;
        this.exerciseDescriptions = exerciseDescriptions;
    }

    public List<ExerciseDescription> getExerciseDescriptions() {
        return exerciseDescriptions;
    }

    public void setExerciseDescriptions(List<ExerciseDescription> exerciseDescriptions) {
        this.exerciseDescriptions = exerciseDescriptions;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    public Integer getSkills_id() {
        return skills_id;
    }

    public void setSkills_id(Integer skills_id) {
        this.skills_id = skills_id;
    }

    public Integer getSkillgroup_id() {
        return skillgroup_id;
    }

    public void setSkillgroup_id(Integer skillgroup_id) {
        this.skillgroup_id = skillgroup_id;
    }
}
