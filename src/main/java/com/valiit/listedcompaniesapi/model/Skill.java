package com.valiit.listedcompaniesapi.model;

public class Skill {
    private Integer id;
    private String skillName;
    private Integer skillgroup_id;

    public Skill(){
    }

    public Skill(Integer id, String skillName, Integer skillgroup_id) {
        this.id = id;
        this.skillName = skillName;
        this.skillgroup_id = skillgroup_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public Integer getSkillgroup_id() {
        return skillgroup_id;
    }

    public void setSkillgroup_id(Integer skillgroup_id) {
        this.skillgroup_id = skillgroup_id;
    }
}
