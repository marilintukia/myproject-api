package com.valiit.listedcompaniesapi.repository;

import com.valiit.listedcompaniesapi.model.*;
import com.valiit.listedcompaniesapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Repository
public class ChildRepository {


    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<SkillGroup> getSkillGroups() {
        return jdbcTemplate.query("select * from skillgroup", (row, rowNum) -> {
            SkillGroup skillGroup = new SkillGroup();
            skillGroup.setId(row.getInt("id"));
            skillGroup.setOmandatudOskus(row.getString("omandatudOskus"));
            skillGroup.setTextDescription(row.getString("textDescription"));
            skillGroup.setAgeGroup(row.getString("ageGroup"));
            List<Skill> skills = getSkills(row.getInt("id"));
            skillGroup.setSkills(skills);
            return skillGroup;
        });
    }


    public List<SkillGroup> getChildSkillGroups(int childId) {
        return jdbcTemplate.query(
                "select * from skillgroup where id in (select skillgroup_id from childskillgroup where child_id = ?)",
                new Object[]{childId},
                (row, rowNum) -> {
                    SkillGroup skillGroup = new SkillGroup();
                    skillGroup.setId(row.getInt("id"));
                    skillGroup.setOmandatudOskus(row.getString("omandatudOskus"));
                    skillGroup.setTextDescription(row.getString("textDescription"));
                    skillGroup.setAgeGroup(row.getString("ageGroup"));
                    List<Skill> skills = getSkills(row.getInt("id"));
                    skillGroup.setSkills(skills);
                    return skillGroup;
                });
    }

    public List<Skill> getSkills(int SkillgroupId) {
        return jdbcTemplate.query("select * from skills where skillgroup_id = ?", new Object[]{SkillgroupId}, (row, rowNum) -> {
            Skill skill = new Skill();
            skill.setId(row.getInt("id"));
            skill.setSkillName(row.getString("skillName"));
            skill.setSkillgroup_id(row.getInt("skillgroup_id"));
            return skill;
        });
    }

    public List<Exercise> getExercises(int ageInMonths) {
        String sql = "SELECT e2.*, filtered_exercises.skillgroup_id FROM exercise e2 INNER JOIN\n" +
                "(\n" +
                "\tSELECT \n" +
                "\t\tsuitable_exercises.id AS id,\n" +
                "\t\tsuitable_exercises.skillgroup_id AS skillgroup_id\n" +
                "\tFROM\n" +
                "\t(\n" +
                "\t\tSELECT\n" +
                "\t\t\te.id AS id,\n" +
                "\t\t\t(SUBSTRING(sg.ageGroup, 1, 2)) AS weeks_from,\n" +
                "\t\t\t(SUBSTRING(sg.ageGroup, 4, 2)) AS weeks_to,\n" +
                "\t\t\tsg.id AS skillgroup_id\n" +
                "\t\tFROM exercise e\n" +
                "\t\tINNER JOIN skills s ON s.id = e.skills_id\n" +
                "\t\tINNER JOIN skillgroup sg ON sg.id = s.skillgroup_id\n" +
                "\t) suitable_exercises\n" +
                "\tWHERE\n" +
                "\t\tsuitable_exercises.weeks_from <= ?\n" +
                "\t\tAND suitable_exercises.weeks_to >= ?\n" +
                ") filtered_exercises ON filtered_exercises.id = e2.id";
        return jdbcTemplate.query(sql,
                new Object[]{ageInMonths, ageInMonths}, (row, rowNum) -> {
                    Exercise exercise = new Exercise();
                    exercise.setId(row.getInt("id"));
                    exercise.setExerciseName(row.getString("exerciseName"));
                    exercise.setSkills_id(row.getInt("skills_id"));
                    exercise.setSkillgroup_id(row.getInt("skillgroup_id"));
                    List<ExerciseDescription> exerciseDescriptions = getExerciseDescriptions(row.getInt("id"));
                    exercise.setExerciseDescriptions(exerciseDescriptions);
                    return exercise;
                });
    }

    public List<ExerciseDescription> getExerciseDescriptions(int ExerciseId) {
        return jdbcTemplate.query("select * from exercisedescription where exercise_id = ?", new Object[]{ExerciseId}, (row, rowNum) -> {
            ExerciseDescription exerciseDescription = new ExerciseDescription();
            exerciseDescription.setId(row.getInt("id"));
            exerciseDescription.setPictureName(row.getString("pictureName"));
            exerciseDescription.setPictureDescription(row.getString("pictureDescription"));
            exerciseDescription.setExercise_id(row.getInt("exercise_id"));
            return exerciseDescription;
        });
    }

//    public SkillGroup getSkillGroup(int id) {
//        List<SkillGroup> companies = jdbcTemplate.query("select * from company where id = ?", new Object[]{id}, mapCompanyRows);
//        return companies.size() > 0 ? companies.get(0) : null;
//    }

    @Autowired
    private UserService userService;

    public void addChild(ChildRegistration childRegistration) {
        User currentlyLoggedInUser = userService.getUser();
        jdbcTemplate.update(
                "insert into child (id, firstName, lastName, picture, age, prematurity, trueAgeInWeeks, user_id) values (?, ?, ?, ?, ?, ?, ?, ?)",
                childRegistration.getId(), childRegistration.getFirstName(), childRegistration.getLastName(), childRegistration.getPicture(), childRegistration.getAge(),
                childRegistration.getPrematurity(), childRegistration.getTrueAgeInWeeks(), currentlyLoggedInUser.getId()
        );
    }

    public void updateChild(ChildRegistration childRegistration) {
        jdbcTemplate.update(
                "update child set firstName = ?, lastName = ?, picture = ?, age = ?, prematurity = ?, trueAgeInWeeks = ? where id = ?",
                childRegistration.getFirstName(), childRegistration.getLastName(), childRegistration.getPicture(), childRegistration.getAge(),
                childRegistration.getPrematurity(), childRegistration.getTrueAgeInWeeks(), childRegistration.getId()
        );
    }

    public void addExercise(int childId, int exerciseId) {
        jdbcTemplate.update("insert into child_exercises (child_id, exercise_id, count) values (?, ?, ?)",
                childId, exerciseId, 1
        );
    }

    public void updateExercise(int count, int childId, int exerciseId) {
        jdbcTemplate.update("update child_exercises set count = ? where child_id = ? and exercise_id = ? ",
                count, childId, exerciseId
        );
    }

    public int getCount(int childId, int exerciseId) {//user exists
        Integer count = jdbcTemplate.queryForObject(
                "select `count` from child_exercises where child_id = ? and exercise_id = ?",
                new Object[]{childId, exerciseId},
                Integer.class
        );
        return count != null ? count : 0;
    }

    public boolean exerciseCountExists(int childId, int exerciseId) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(*) from child_exercises where child_id = ? and exercise_id = ?",
                new Object[]{childId, exerciseId},
                Integer.class
        );
        return count != null ? count > 0 : false;
    }

//    public boolean userExists(String username) {
//        Integer count = jdbcTemplate.queryForObject(
//                "select count(id) from user where username = ?",
//                new Object[]{username},
//                Integer.class
//        );
//        return count != null && count > 0;
//    }

    public List<ChildRegistration> getAllChildren() {
        User currentlyLoggedInUser = userService.getUser();
//        return jdbcTemplate.query("select *, ( FLOOR(DATEDIFF(DATE_ADD(NOW(), INTERVAL child.prematurity WEEK), NOW()) / 7) ) as ageInWeeks from child where user_id=?", new Object[]{currentlyLoggedInUser.getId()}, (row, rowNum) -> {
        return jdbcTemplate.query("select * from child where user_id = ?", new Object[]{currentlyLoggedInUser.getId()}, (row, rowNum) -> {
                    ChildRegistration child = new ChildRegistration();
                    child.setId(row.getInt("id"));
                    child.setFirstName(row.getString("firstName"));
                    child.setLastName(row.getString("lastName"));
                    child.setAge(row.getDate("age").toLocalDate());
                    child.setPicture(row.getString("picture"));
                    child.setPrematurity(row.getInt("prematurity"));
                    child.setPrematurityAgeInWeeks((int) ChronoUnit.WEEKS.between(row.getDate("age").toLocalDate().plusWeeks(row.getInt("prematurity")), LocalDate.now()));
                    child.setTrueAgeInWeeks((int) ChronoUnit.WEEKS.between(row.getDate("age").toLocalDate(), LocalDate.now()));
                    List<SkillGroup> childSkills = getChildSkillGroups(row.getInt("id"));
                    child.setChildSkills(childSkills);
                   List<ChildRegistration.ExerciseCount> countList = getExerciseCounts(row.getInt("id"));
                   child.setExerciseCount(countList);
                    return child;
                }
        );
    }

    public List<ChildRegistration.ExerciseCount> getExerciseCounts(int childId) {
        return jdbcTemplate.query("select * from child_exercises where child_id = ?", new Object[]{childId},
                (rs, rowNum) -> {
                    ChildRegistration.ExerciseCount exerciseCount = new ChildRegistration.ExerciseCount();
                    exerciseCount.setExercise_id(rs.getInt("exercise_id"));
                    exerciseCount.setCount(rs.getInt("count"));
                    return exerciseCount;
                }
        );
    }

    public void deleteChild(int id) {
        jdbcTemplate.update("delete from childSkillGroup where child_id = ?", id);
        jdbcTemplate.update("delete from child where id = ?", id);
    }

    public void deleteSkill(int childId, int skillgroupId) {
        jdbcTemplate.update("delete from childSkillGroup where child_id = ? and skillgroup_id = ?", childId, skillgroupId);
    }

    public void saveSkill(int childId, int skillgroupId) {
        jdbcTemplate.update("insert into childskillgroup (child_id, skillgroup_id) values (?, ?)",
                childId, skillgroupId
        );
    }

}
